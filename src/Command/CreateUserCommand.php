<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:create-user',
    description: 'Add a short description for your command',
)]
class CreateUserCommand extends Command
{
    private EntityManager $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->entityManager->getConnection()->getConfiguration()->setSQLLogger(null);

        $batchSize = 20;

        for ($i = 0; $i < 100000; $i++) {
            $user = new User();
            $user->setUsername('user'.random_int(10000000000, 99999999999))
                ->setScore(random_int(0, 1000))
                ->setLog((bool)random_int(0, 1));
            $this->entityManager->persist($user);

            if (($i % $batchSize) === 0) {
                $this->entityManager->flush();
                $this->entityManager->clear(); // Detaches all objects from Doctrine!
            }
        }
        $this->entityManager->flush();
        $this->entityManager->clear();

        return Command::SUCCESS;
    }
}
