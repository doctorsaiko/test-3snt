<?php

namespace App\Command;

use App\Entity\User;
use App\Entity\UserLog;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:update-user-log',
    description: 'Update users log',
)]
class UpdateUserLogCommand extends Command
{
    public function __construct(private EntityManagerInterface $em)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /*
         * https://www.doctrine-project.org/projects/doctrine-orm/en/2.11/reference/batch-processing.html#iterating-results
         */
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        $batchSize = 20;
        $i = 1;
        $dql = 'select u from '.User::class.' u';
        $q = $this->em->createQuery($dql);
        foreach ($q->toIterable() as $user) {
            if ($user->getLog()) {
                $user->setLog(false);
                if ($user->getScore() < 100 || $user->getScore() > 900) {
                    $user->setLog(true);
                    $userLog = new UserLog();
                    $userLog->setUserId($user->getId());
                    $userLog->setScore($user->getScore());
                    $userLog->setCreatedAt(new \DateTimeImmutable());

                    $this->em->persist($userLog);
                }
            }
            ++$i;
            if (($i % $batchSize) === 0) {
                $this->em->flush();
                $this->em->clear();
            }
        }
        $this->em->flush();

        return Command::SUCCESS;
    }
}
