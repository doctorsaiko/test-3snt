<?php

namespace App\EventSubscriber;

use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Security\Core\Security;

class UserLogRequestSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private Security $security,
        private LoggerInterface $userRequestLogger,
        private ParameterBagInterface $params
    ) {
    }

    public static function getSubscribedEvents(): array|string
    {
        return [
            RequestEvent::class => 'onKernelRequest',
        ];
    }


    public function onKernelRequest(RequestEvent $event): void
    {
        if (!$this->params->get('enable_user_log')) {
            return;
        }

        if (!$user = $this->security->getUser()) {
            return;
        }

        if (!$user->getLog()) {
            return;
        }

        $methods = [Request::METHOD_GET, Request::METHOD_POST];
        $request = $event->getRequest();

        if (in_array($request->getMethod(), $methods)) {
            $requestInfo = 'user_id: '.$user->getId().'/ '.$request;
            $this->userRequestLogger->log(LogLevel::INFO, $requestInfo);
        }
    }
}